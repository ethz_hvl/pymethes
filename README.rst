pyMETHES
========


.. image:: https://gitlab.com/ethz_hvl/pymethes/badges/master/pipeline.svg
        :target: https://gitlab.com/ethz_hvl/pymethes/commits/master
        :alt: Pipeline status

.. image:: https://gitlab.com/ethz_hvl/pymethes/badges/master/coverage.svg
        :target: https://gitlab.com/ethz_hvl/pymethes/commits/master
        :alt: Coverage report

.. image:: https://readthedocs.org/projects/pymethes/badge/?version=stable
        :target: https://pymethes.readthedocs.io/en/latest/?badge=stable
        :alt: Documentation Status


A Monte Carlo simulation of electron transport in low temperature plasmas.

Python version of the MATLAB's `METHES`_ package

.. _METHES: https://doi.org/10.3929/ethz-a-010881688


Documentation
-------------
read `pyMETHES documentation at RTD`_


.. _`pyMETHES documentation at RTD`: https://readthedocs.org/projects/pymethes/


License
-------
GNU General Public License v3 (GPLv3)

