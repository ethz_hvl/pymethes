#  Copyright (c) 2020-2021 ETH Zurich

__title__ = "pyMETHES"
__summary__ = "Monte Carlo simulation of electron transport in low temperature plasmas"
__url__ = "https://gitlab.com/ethz_hvl/pymethes/"
__version__ = "0.1.1"
__author__ = "Alise Chachereau, Kerry Jansen, Markus Niese, Martin Vahlensieck"
__email__ = "alisec@ethz.ch"
__license__ = "GNU General Public License v3 (GPLv3)"
__copyright__ = "Copyright (c) 2020-2021 ETH Zurich"
