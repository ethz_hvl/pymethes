{
  input_gases: {
    gases: [
      "Ar",
      "N2"
    ],
    paths_to_cross_section_files:  [
      "cross_sections/Ar_Biagi.txt",
      "cross_sections/N2_Biagi.txt"
    ],
    fractions:  [
    0.3,
    0.7
    ],
    // maximum cross section energy (eV). If a cross section stops before that energy,
    // a data point is appended at that energy with the last value of the cross section.
    // If a cross section extends beyond that energy, these data points are ignored.
    max_cross_section_energy: 1e3
  },
  output: {
    output_directory: "results/",
    // base name for all output files
    base_name: 'Ar_N2',
    // saves the whole simulation as a pickle
    save_simulation_pickle: true,
    // saves the temporal evolution of the swarm data to a json file
    save_temporal_evolution: true,
    // saves the time-averaged values of the swarm parameters to a json file
    save_swarm_parameters: true,
    // saves the time-averaged eedf and eepf to a json file
    save_energy_distribution: true,
  },
  physical_conditions: {
    // E/N in Townsend
    EN: 100,
    // gas pressure in Pascal
    pressure: 1e6,
    // gas temperature in Kelvin
    temperature: 300
  },
  initial_state: {
    // initial electron number
    num_e_initial: 1e4,
    // initial position of center of mass of gaussian distributed electrons
    // in x, y and z directions
    initial_pos_electrons: [0, 0, 0],
    // initial broadening of gaussian distributed electrons in x, y and z directions
    initial_std_electrons: [0, 0, 0]
  },
  simulation_settings: {
    // number of energy bins for the electron energy distribution
    num_energy_bins: 2000,
    // energy sharing factor in interval [0,1]
    energy_sharing_factor: 0.5,
    // scattering: isotropic (true), non-isotropic according to Vahedi et al. (false)
    isotropic_scattering: true,
    // conserve the electron number in case of ionization or attachment collisions
    conserve: false,
    // maximum allowed electron number (when it is reached, the number of electrons is
    // then conserved until simulation ends)
    num_e_max: 5e6,
  },
  end_conditions: {
    // error tolerance for the flux drift velocity
    w_tol: 0.02,
    // error tolerance for the flux diffusion coefficient
    DN_tol: 0.02,
    // maximum number of collisions of simulation
    num_col_max: 1e7,
    // the simulation also ends if the number of electrons drops to zero
  }
}
