"""
Example script using a custom end condition. The end condition used
is the old default.
"""

import numpy as np
from pyMETHES import Config, Simulation


def custom_callback(sim) -> bool:
    """
    Check end conditions for the simulation.

    Returns: True if simulation is finished, False otherwise.
    """

    end = False

    # condition on the convergence of flux w and flux DN
    if not np.isnan(sim.output.flux.DN_err[2]):
        w = sim.output.flux.w
        w_err = sim.output.flux.w_err
        DN = sim.output.flux.DN
        DN_err = sim.output.flux.DN_err
        if w_err[2] <= w[2] * sim.config.w_tol \
                and all(DN_err <= DN * sim.config.DN_tol):
            end = True
            print("Simulation ended."
                  f" Error of w < {100 * sim.config.w_tol:5.3}%."
                  f" Error of DN < {100 * sim.config.DN_tol:5.3}%.")

    if sim.output.time_series.num_collisions[-1] >= sim.config.num_col_max:
        end = True
        print('Simulation ended: maximum number of collisions reached')

    return end


# load the settings

cfg = Config("config/Ar_N2.json5")
cfg.end_condition_type = "custom"
cfg.is_done = custom_callback
sim = Simulation(cfg.to_dict())

# run the simulation
sim.run()
