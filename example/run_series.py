"""
Example script to run a series of simulations, varying one configuration
parameter.
"""

import numpy as np
from pyMETHES import Simulation

# load the base settings
sim = Simulation("config/CO2.json5")

# run a series of simulations by providing an array of E/N values
EN_values = np.arange(start=5, stop=150, step=5)
sim.run_series('EN', EN_values)
