=======
Authors
=======

`pyMETHES` was written by:

* Alise Chachereau <alisec@ethz.ch>, and
* Kerry Jansen <kjansen@student.ethz.ch>, and
* Markus Niese <mniese@student.ethz.ch>, and
* Martin Vahlensieck <martinva@student.ethz.ch>

Contributors:

 * Mikolaj Rybinski <mikolaj.rybinski@id.ethz.ch>
