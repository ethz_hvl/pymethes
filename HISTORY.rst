=======
History
=======

0.1.0 (2020-06-19)
------------------

* Simulation in homogeneous electric field
* Parametric sweep over one configuration parameter

0.1.1 (2020-06-24)
------------------

* Fix flow diagram not appearing in documentation
* More detailed USAGE.rst and README.rst
* Set electron scattering model to isotropic in the example configuration (anisotropic scattering is untested)


0.2.0 (2021-01-20)
------------------

* Added new configuration options:
    * Added a timeout for the simulation
    * Option for seeding random numbers (defaut: 'random')
    * Options to specify initial energy of electrons, including Maxwell-Boltzmann distributed values
    * New end conditions to choose from (**WARNING**: the default end condition has changed, the previous behaviour can be obtained by using the provided example `run_with_custom_end_condition.py`)
* Diverse minor fixes and improvements
    * Replaced outdated links to previous repository
    * Raise TypeError when Simulation configuration is None
    * Diverse fixes and improvements to Makefile
    * Improved gitlab-ci.yaml (added tests for Python 3.8 and 3.9, pylint)
    * Unified docstring style to google style docstrings
    * Improved documentation and code of config.py
* Parametrized and extended tests
