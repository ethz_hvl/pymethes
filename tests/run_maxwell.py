#  Copyright (c) 2020-2021 ETH Zurich

"""
Script to run the simulation of the Maxwell test.
"""

import numpy as np
import scipy.constants as csts
from pyMETHES import Simulation

name = "maxwell"
sim = Simulation("tests/test_data/maxwell.json5")
mass_ratio = 2 * csts.electron_mass / (4 * csts.proton_mass)
sim.gas_mixture.mass_ratios = np.array([mass_ratio])

sim.run()

mid = sim.output.energy_distribution.energy_mean
e_mean = 0.5064
res = 'FAIL'
if np.isclose(mid, e_mean, atol=0, rtol=0.03):
    res = 'PASS'
print(f"e mean = {mid}, expected {e_mean},  rtol {np.abs(mid-e_mean)/e_mean:.4}, {res}")
w = [0.0, 0.0, 4.943e3]
DN = [1.668e24, 1.668e24, 1.668e24]
for i in range(3):
    mid = sim.output.flux.w[i]
    res = 'FAIL'
    if np.isclose(mid, w[i], atol=150, rtol=0.01):
        res = 'PASS'
    print(f"flux w = {mid:.4}, expected {w[i]:.4}, atol {np.abs(mid-w[i]):.4}, {res}")
for i in range(3):
    mid = sim.output.flux.DN[i]
    res = 'FAIL'
    if np.isclose(mid, DN[i], atol=0, rtol=0.06):
        res = 'PASS'
    print(f"flux DN = {mid:.4}, expected {DN[i]:.4}, "
          f"rtol {np.abs(mid-DN[i])/DN[i]:.4}, {res}")

sim.plot_all()
