#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the Output class.
"""

# Import Packages
import pytest
import numpy as np

# Import modules
from pyMETHES.output import Output
from pyMETHES.config import Config
from pyMETHES.electrons import Electrons

np.seterr(all='raise')


@pytest.fixture(scope='module')
def params(tmp_path_factory):
    config = Config('tests/data/config/test_config.json5')
    config.output_directory = str(tmp_path_factory.mktemp('results'))
    version = 'pyMETHES test'
    electrons = Electrons(1000, [0, 0, 0], [1, 1, 0], 3)
    electrons.velocity = np.random.rand(3, 1000)
    return config, version, electrons


def test_instantiation(params):

    Output(*params)


def test_check_sst(params):
    output = Output(*params)
    output.time_series.time = np.array(range(100))
    output.time_series.mean_energy = np.array(range(100, 1, -1))
    assert output.check_sst()
    assert output.time_series.ind_equ == 100


def test_temporal_evolution(params):
    output = Output(*params)
    output.save_temporal_evolution()
    output.time_series.ind_equ = None
    output.plot_temporal_evolution(block=False)
    output.time_series.ind_equ = 0
    output.plot_temporal_evolution(block=False)


def test_save_swarm_parameters(params):
    output = Output(*params)
    output.energy_distribution.energy_mean = 1
    output.energy_distribution.energy_mean_err = 1
    output.flux.w = np.ones((3,))
    output.flux.w_err = np.ones((3,))
    output.flux.DN = np.ones((3,))
    output.flux.DN_err = np.ones((3,))
    output.bulk.w = np.ones((3,))
    output.bulk.w_err = np.ones((3,))
    output.bulk.DN = np.ones((3,))
    output.bulk.DN_err = np.ones((3,))
    output.rates_conv.ionization = 1
    output.rates_conv.attachment = 1
    output.rates_conv.effective = 1
    output.rates_count.ionization = 1
    output.rates_count.attachment = 1
    output.rates_count.effective = 1
    output.rates_count.ionization_err = 1
    output.rates_count.attachment_err = 1
    output.rates_count.effective_err = 1
    output.save_swarm_parameters()


def test_energy_distribution(params):
    output = Output(*params)
    output.energy_distribution.energy_bins = np.linspace(0, 1, 10)
    output.energy_distribution.calculate_bin_centers()
    output.energy_distribution.collect_histogram(np.random.random(100))
    output.energy_distribution.calculate_distribution(np.arange(10))
    output.save_energy_distribution()
    output.plot_energy_distribution(block=False)
