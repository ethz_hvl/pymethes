#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the BulkData and FluxData classes.
"""

# Import Packages
import pytest
import numpy as np
import scipy.constants as csts

# Import modules
from pyMETHES.transport_data import BulkData, FluxData
from pyMETHES.temporal_evolution import TimeSeries
from pyMETHES.electrons import Electrons

np.seterr(all='raise')


@pytest.fixture(scope='module')
def electrons():
    e_field = 3 * csts.electron_mass / csts.elementary_charge
    elec = Electrons(1000, [0, 0, 0], [1, 1, 0], e_field)
    elec.velocity = np.ones((3, 1000))
    return elec


@pytest.fixture(scope='module')
def time_series():
    ts = TimeSeries(Electrons(1000, [0, 0, 0], [1, 1, 0], 3))
    ts.ind_equ = 0
    ts.time = np.array(range(100)) * 0.1
    ts.mean_position = np.repeat(ts.time.reshape(100, 1), 3, axis=1)
    ts.var_position = np.repeat(2 * ts.time.reshape(100, 1), 3, axis=1)
    ts.mean_velocity = np.ones((100, 3))
    ts.mean_velocity_moment = ts.mean_velocity * ts.mean_position
    return ts


def test_instantiation():
    BulkData(1)
    FluxData(1)


def test_bulk_calculate_data(time_series):
    bulk = BulkData(1)
    bulk.calculate_data(time_series)
    assert np.isclose(bulk.w, 1).all()
    assert np.isclose(bulk.w_err, 0).all()
    assert np.isclose(bulk.DN, 1).all()
    assert np.isclose(bulk.DN_err, 0).all()


def test_flux_calculate_data(time_series):
    flux = FluxData(1)
    flux.calculate_data(time_series)
    assert all(np.isclose(flux.w, 1))
    assert all(np.isclose(flux.DN, 0))
    assert all(np.isclose(flux.w_err, 0))
    assert all(np.isclose(flux.DN_err, 0))
