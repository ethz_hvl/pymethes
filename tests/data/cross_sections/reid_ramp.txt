Data printed using the package 'lxcat_data_parser', formatted  in accordance with the cross section data format of LXCat, www.lxcat.net.

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
DATABASE: Dummy database
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

********************************
ELASTIC
Ar
0.000272308510742333
PROCESS: Elastic collision
COLUMNS: Energy (eV) | Cross section (m2)
-----------------------------
0.000000e+00	6.000000e-20
1.000000e+01	6.000000e-20
-----------------------------

EXCITATION
Ar
0.2
PROCESS: Vibrational excitation
COLUMNS: Energy (eV) | Cross section (m2)
-----------------------------
2.000000e-01	0.000000e+00
1.000000e+01	9.800000e-19
-----------------------------

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
