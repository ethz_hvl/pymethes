#  Copyright (c) 2020-2021 ETH Zurich

"""
Script to generate cross section files for the Maxwell and Reid Ramp tests.
"""

import numpy as np
import scipy.constants as csts
import lxcat_data_parser as ldp
import pandas as pd

# create set of cross section for Maxwell test
xset = ldp.CrossSectionSet('Ar.txt')
x = np.logspace(-6, 1, num=1000) - 1e-6
x[-1] = 10
y = 6e-20 / np.sqrt(np.clip(x, a_min=1.0e-8, a_max=None))
xset.cross_sections = [xset.cross_sections[0]]
xset.cross_sections[0].data = pd.DataFrame({
    "energy": x,
    "cross section": y
})
xset.cross_sections[0].type = ldp.CrossSectionTypes.ELASTIC
xset.cross_sections[0].mass_ratio = 2 * csts.electron_mass / (4 * csts.proton_mass)
xset.write('maxwell.txt')

# create set of cross section for Reid Ramp test
xset = ldp.CrossSectionSet('test_data/Ar.txt')
xset.cross_sections = [xset.cross_sections[0], xset.cross_sections[1]]
xset.cross_sections[0].data = pd.DataFrame({
    "energy": [0.0, 10.0],
    "cross section": [6e-20, 6e-20]
})
xset.cross_sections[0].type = ldp.CrossSectionTypes.ELASTIC
xset.cross_sections[0].mass_ratio = 2 * csts.electron_mass / (4 * csts.proton_mass)
x = np.array([0.2, 10])
xset.cross_sections[1].data = pd.DataFrame({
    "energy": x,
    "cross section": (x - 0.2) * 10e-20
})
xset.cross_sections[1].type = ldp.CrossSectionTypes.EXCITATION
xset.cross_sections[1].threshold = 0.2
xset.write('reid_ramp.txt')
