#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the Config class.
"""

# Import Packages
import pytest
import numpy as np
import scipy.constants as csts

# Import modules
from pyMETHES.config import Config

np.seterr(all='raise')


@pytest.fixture(scope='module')
def config(tmp_path_factory):
    cfg = Config('tests/data/config/test_config.json5')
    cfg.output_directory = str(tmp_path_factory.mktemp('results'))
    return cfg


def sample_callback():
    return True


def test_config():
    # reading from a json5 file
    cfg = Config('tests/data/config/test_config.json5')

    # reading from a json file
    cfg2 = Config('tests/data/config/test_config.json')

    # invalid extension
    with pytest.raises(ValueError):
        Config('invalid.config')

    # exporting to dictionary
    cfg_dict = cfg.to_dict()
    cfg_dict2 = cfg2.to_dict()
    assert isinstance(cfg_dict, dict)
    assert cfg_dict == cfg_dict2

    # check the values
    assert cfg._gas_number_density is None
    n = cfg.pressure / (csts.Boltzmann * cfg.temperature)
    assert np.isclose(cfg.gas_number_density, n)
    assert np.isclose(cfg._gas_number_density, n)

    # check that the cache is reset:
    cfg.pressure = 1e3
    assert cfg._gas_number_density is None
    n = cfg.pressure / (csts.Boltzmann * cfg.temperature)
    assert np.isclose(cfg.gas_number_density, n)
    assert np.isclose(cfg._gas_number_density, n)

    # check that the cache is reset:
    cfg.temperature = 295
    assert cfg._gas_number_density is None
    n = cfg.pressure / (csts.Boltzmann * cfg.temperature)
    assert np.isclose(cfg.gas_number_density, n)
    assert np.isclose(cfg._gas_number_density, n)


def test_save(config):
    with pytest.warns(None) as record:
        config.save_json(f"{config.output_directory}/cfg.json")
        config.save_json5(f"{config.output_directory}/cfg.json5")

    assert len(record) == 0

    config.is_done = sample_callback

    with pytest.warns(UserWarning, match="Cannot save custom callback to json"):
        config.save_json(f"{config.output_directory}/cfg.json")

    with pytest.warns(UserWarning, match="Cannot save custom callback to json"):
        config.save_json5(f"{config.output_directory}/cfg.json5")


def test_seed_setting(config):
    cfg = config.to_dict()
    if 'seed' in cfg['simulation_settings']:
        del cfg['simulation_settings']['seed']
    assert Config(cfg).seed == "random"

    cfg['simulation_settings']['seed'] = 15
    c = Config(cfg)
    assert c.seed == 15
    cfg2 = c.to_dict()
    assert cfg2['simulation_settings']['seed'] == 15

    cfg['simulation_settings']['seed'] = 15.0
    with pytest.raises(ValueError):
        Config(cfg)

    cfg['simulation_settings']['seed'] = "random"
    c = Config(cfg)
    assert c.seed == "random"
    cfg2 = c.to_dict()
    assert cfg2['simulation_settings']['seed'] == "random"

    cfg['simulation_settings']['seed'] = "notrandom"
    with pytest.raises(ValueError):
        Config(cfg)

    cfg['simulation_settings']['seed'] = None
    with pytest.raises(ValueError):
        Config(cfg)


def test_initial_energy_distribution(config):
    cfg = config.to_dict()

    for key in ['initial_energy_distribution', 'initial_energy', 'initial_direction']:
        if key in cfg['initial_state']:
            del cfg['initial_state'][key]

    c = Config(cfg)
    assert c.initial_energy_distribution == "zero"
    assert c.initial_energy is None
    assert c.initial_direction is None
    cfg2 = c.to_dict()
    assert cfg2['initial_state']['initial_energy_distribution'] == "zero"
    assert cfg2['initial_state']['initial_energy'] is None
    assert cfg2['initial_state']['initial_direction'] is None

    cfg['initial_state']['initial_energy_distribution'] = "invalid"

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_temperature'] = 300
    for dist in ("zero", "maxwell-boltzmann"):
        cfg['initial_state']['initial_energy_distribution'] = dist
        c = Config(cfg)
        assert c.initial_energy_distribution == dist
        assert c.initial_energy is None
        assert c.initial_direction is None
        cfg2 = c.to_dict()
        assert cfg2['initial_state']['initial_energy_distribution'] == dist
        assert cfg2['initial_state']['initial_energy'] is None
        assert cfg2['initial_state']['initial_direction'] is None

        # useless setting of initial_energy
        cfg['initial_state']['initial_energy'] = 5
        expected = f"initial_energy setting useless with {dist} distribution"
        with pytest.warns(UserWarning, match=expected):
            Config(cfg)
        del cfg['initial_state']['initial_energy']

        # useless setting of initial_direction
        cfg['initial_state']['initial_direction'] = "random"
        expected = f"initial_direction setting useless with {dist} distribution"
        with pytest.warns(UserWarning, match=expected):
            Config(cfg)
        del cfg['initial_state']['initial_direction']

    cfg['initial_state']['initial_energy_distribution'] = "maxwell-boltzmann"
    cfg['initial_state']['initial_temperature'] = -1

    with pytest.raises(ValueError):
        Config(cfg)

    del cfg['initial_state']['initial_temperature']

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_energy_distribution'] = "fixed"

    # No energy and no direction
    with pytest.raises(ValueError):
        Config(cfg)

    # Energy and no direction
    cfg['initial_state']['initial_energy'] = 5

    with pytest.raises(ValueError):
        Config(cfg)

    # Energy and direction
    cfg['initial_state']['initial_direction'] = "random"

    c = Config(cfg)
    cfg = c.to_dict()
    assert cfg['initial_state']['initial_direction'] == "random"


def test_initial_energy(config):
    cfg = config.to_dict()

    cfg['initial_state']['initial_energy_distribution'] = "fixed"
    cfg['initial_state']['initial_direction'] = "random"
    cfg['initial_state']['initial_energy'] = "invalid"

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_energy'] = -1

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_energy'] = -1

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_energy'] = 5
    c = Config(cfg)
    assert c.initial_energy_distribution == "fixed"
    assert c.initial_energy == 5.0
    cfg = c.to_dict()
    assert cfg['initial_state']['initial_energy_distribution'] == "fixed"
    assert cfg['initial_state']['initial_energy'] == 5.0


def test_initial_direction(config):
    cfg = config.to_dict()

    cfg['initial_state']['initial_energy_distribution'] = "fixed"
    cfg['initial_state']['initial_energy'] = 5
    cfg['initial_state']['initial_direction'] = "invalid"

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_direction'] = 15

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_direction'] = [1, 2]

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_direction'] = ["invalid", "invalid", "invalid"]

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_direction'] = [0, 0, 0]

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['initial_state']['initial_direction'] = [1, 1, 1]

    c = Config(cfg)
    assert c.initial_energy_distribution == "fixed"
    assert c.initial_energy == 5.0
    assert c.initial_direction == [1, 1, 1]
    cfg = c.to_dict()
    assert cfg['initial_state']['initial_direction'] == [1, 1, 1]


def test_end_condition_settings(config):
    cfg = config.to_dict()

    for key in ('end_condition_type', 'is_done'):
        if key in cfg['end_conditions']:
            del cfg['end_conditions'][key]

    c = Config(cfg)
    assert c.end_condition_type == "w_tol+ND_tol"
    assert c.to_dict()['end_conditions']['end_condition_type'] == "w_tol+ND_tol"

    cfg['end_conditions']['end_condition_type'] = "invalid"

    with pytest.raises(ValueError):
        Config(cfg)

    for t in ('steady-state', 'num_col_max', 'w_tol+ND_tol'):
        cfg['end_conditions']['end_condition_type'] = t
        c = Config(cfg)
        assert c.end_condition_type == t
        assert c.to_dict()['end_conditions']['end_condition_type'] == t

    cfg['end_conditions']['end_condition_type'] = "custom"

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['end_conditions']['is_done'] = "invalid"

    with pytest.raises(TypeError):
        Config(cfg)

    cfg['end_conditions']['is_done'] = sample_callback
    c = Config(cfg)
    assert c.end_condition_type == "custom"
    assert c.is_done == sample_callback
    cfg2 = c.to_dict()
    assert cfg2['end_conditions']['end_condition_type'] == "custom"
    assert cfg2['end_conditions']['is_done'] == sample_callback

    # Set is_done without end_condition_type == "callback"
    cfg['end_conditions']['end_condition_type'] = "num_col_max"
    expected = "Setting is_done is useless without end_condition_type custom"
    with pytest.warns(UserWarning, match=expected):
        Config(cfg)


def test_timeout(config):
    cfg = config.to_dict()
    cfg['end_conditions']['timeout'] = "invalid"

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['end_conditions']['timeout'] = -1

    with pytest.raises(ValueError):
        Config(cfg)

    cfg['end_conditions']['timeout'] = 5

    assert Config(cfg).timeout == 5
