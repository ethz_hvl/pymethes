#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the InterpolatedCrossSectionSet class.
"""

# Import packages
import pytest
import numpy as np
from lxcat_data_parser import (
    CrossSectionReadingError,
    CrossSectionTypes
)

# Import modules
from pyMETHES.cross_section import InterpolatedCrossSectionSet

np.seterr(all='raise')


def test_instantiation():

    max_energy = 1000
    params = [max_energy, "tests/data/cross_sections/Ar.txt", "Ar"]
    gas = InterpolatedCrossSectionSet(*params)
    assert np.isclose(gas.cross_sections[0].mass_ratio, 2.746e-05)
    # check the values added at zero and max_energy
    for x in gas.cross_sections:
        assert x.interpolation(0) == x.interpolation(1)
        assert x.interpolation(max_energy) == x.interpolation(2)
        assert x.interpolation.x[0] == 0
        assert x.interpolation.x[-1] == max_energy

    params = [max_energy, "tests/data/cross_sections/Ar.txt", "SpeciesNotInFile"]
    with pytest.raises(CrossSectionReadingError):
        InterpolatedCrossSectionSet(*params)

    params = [max_energy,
              "tests/data/cross_sections/wrong_notaformula.txt", "notaformula"]

    expected = "Could not check the input mass ratio using the molmass package. " \
               "notaformula is not a valid chemical formula."

    with pytest.warns(UserWarning, match=expected):
        InterpolatedCrossSectionSet(*params)

    params = [max_energy,
              "tests/data/cross_sections/wrong_CO2_elastic_and_effective.txt", "CO2"]
    with pytest.raises(CrossSectionReadingError):
        InterpolatedCrossSectionSet(*params)

    params = [max_energy,
              "tests/data/cross_sections/wrong_CO2_no_elastic_or_effective.txt", "CO2"]
    with pytest.raises(CrossSectionReadingError):
        InterpolatedCrossSectionSet(*params)

    params = [max_energy,
              "tests/data/cross_sections/wrong_Ar_mass_ratio.txt", "Ar"]

    expected = "Incorrect mass ratio. The mass ratio 1.24e-05 read from the file " \
               "'tests/data/cross_sections/wrong_Ar_mass_ratio.txt' for the species " \
               "Ar does not match the value 1.373e-05 calculated with the molmass " \
               "package. You may consider fixing the input file"

    with pytest.warns(UserWarning, match=expected):
        InterpolatedCrossSectionSet(*params)


def test_effective_to_elastic():

    max_energy = 1000
    params = [max_energy, "tests/data/cross_sections/CO2.txt", "CO2"]
    gas = InterpolatedCrossSectionSet(*params)

    # check that the EFFECTIVE cross section has been removed
    assert CrossSectionTypes.EFFECTIVE not in [
        x.type for x in gas.cross_sections]

    # check that there is one (and only one) elastic cross section
    ela = [x for x in gas.cross_sections if x.type == CrossSectionTypes.ELASTIC]
    assert len(ela) == 1

    # check the values of the elastic cross section
    ela = ela[0]
    assert np.isclose(ela.interpolation(1), 4e-20)
    assert np.isclose(ela.interpolation(2), 2e-20)

    # check the values added at zero and max_energy
    for x in gas.cross_sections:
        assert x.interpolation(0) == x.interpolation(1)
        assert x.interpolation(max_energy) == x.interpolation(2)


def test_plot():

    max_energy = 1000
    params = [max_energy, "tests/data/cross_sections/Ar.txt", "Ar"]
    gas = InterpolatedCrossSectionSet(*params)
    gas.plot(block=False)
