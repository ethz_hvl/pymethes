#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the utils module.
"""

# Import Packages
import numpy as np
import scipy.constants as csts

# Import modules
import pyMETHES.utils as utils


def test_velocity_from_energy():

    energy = np.array([0, 12, 37])
    assert (utils.velocity_from_energy(energy) == np.sqrt(
        (2 * energy * csts.elementary_charge) / csts.electron_mass)).all()


def test_energy_from_velocity():

    velocity = np.array([0, 12, 37])
    assert (utils.energy_from_velocity(velocity) ==
            0.5 * csts.electron_mass * velocity ** 2 / csts.elementary_charge).all()


def test_acceleration_from_electric_field():

    e_field = np.array([-1, 0, 3])

    acceleration = e_field * csts.elementary_charge / csts.electron_mass
    assert (utils.acceleration_from_electric_field(e_field) == acceleration).all()


def test_maxwell_boltzmann_eedf():

    energies = np.arange(0, 0.15, 0.01)
    expected = [0.00000000e+00, 4.22311522e-07, 5.51052163e-07, 4.67029579e-07,
                3.31716657e-07, 2.13868630e-07, 1.29698139e-07, 7.53995434e-08,
                4.24982869e-08, 2.33945661e-08, 1.26405514e-08, 6.72774984e-09,
                3.53644952e-09, 1.83960851e-09, 9.48456076e-10]

    assert np.allclose(utils.maxwell_boltzmann_eedf(energies, 300), expected)


def test_maxwell_boltzmann_random():

    for t in (10, 300, 1000):
        values = utils.maxwell_boltzmann_random(10000, t)
        assert np.isclose(np.mean(values), 3/2 * csts.k * t / csts.e, atol=0.03)
