#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the GasMixture class.
"""

# Import packages
import pytest
import numpy as np

# Import modules
from pyMETHES.gas_mixture import GasMixture

np.seterr(all='raise')


def test_instantiation():

    species = ["Ar", "CO2"]
    paths = ["tests/data/cross_sections/Ar.txt", "tests/data/cross_sections/CO2.txt"]
    proportions = [0.3, 0.7]
    max_energy = 1000

    gm = GasMixture(species, paths, proportions, max_energy)
    assert np.isclose(gm.total_cross_section[0], 10e-20)

    proportions = [0.3, 0.6]

    with pytest.raises(ValueError):
        GasMixture(species, paths, proportions, max_energy)
