#  Copyright (c) 2020-2021 ETH Zurich

"""
Script to plot the results of the Reid Ramp test.
"""

import pickle
import numpy as np

with open("tests/test_data/reid_ramp_simulation.pickle", "rb") as pickle_file:
    sim = pickle.load(pickle_file)

mid = sim.output.energy_distribution.energy_mean
e_mean = 0.2689
res = 'FAIL'
if np.isclose(mid, e_mean, atol=0, rtol=0.01):
    res = 'PASS'
print(f"e mean = {mid}, expected {e_mean},  rtol {np.abs(mid-e_mean)/e_mean:.4}, {res}")
w = [0.0, 0.0, 6.838e4]
DN = [1.135e24, 1.135e24, 0.5688e24]
for i in range(3):
    mid = sim.output.flux.w[i]
    res = 'FAIL'
    if np.isclose(mid, w[i], atol=400, rtol=0.01):
        res = 'PASS'
    print(f"flux w = {mid:.4}, expected {w[i]:.4}, atol {np.abs(mid-w[i]):.4}, {res}")
for i in range(3):
    mid = sim.output.flux.DN[i]
    res = 'FAIL'
    if np.isclose(mid, DN[i], atol=0, rtol=0.08):
        res = 'PASS'
    print(f"flux DN = {mid:.4}, expected {DN[i]:.4}, "
          f"rtol {np.abs(mid-DN[i])/DN[i]:.4}, {res}")

sim.plot_all()
