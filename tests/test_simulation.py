#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the Simulation class.
"""

# Import Packages
import copy
import time
import pytest
import numpy as np

# Import modules
from pyMETHES import Config, Simulation, utils

np.seterr(all='raise')


@pytest.fixture(scope='function')
def sim(tmp_path_factory):
    sim = Simulation('tests/data/config/test_config.json5')
    sim.config.output_directory = str(tmp_path_factory.mktemp('results'))
    single = np.arange(1, 101)
    triple = np.ones((100, 3))
    sim.output.time_series.time = single
    sim.output.time_series.mean_position = triple
    sim.output.time_series.var_position = triple
    sim.output.time_series.mean_velocity = triple
    sim.output.time_series.mean_velocity_moment = triple
    sim.output.time_series.mean_energy = single
    sim.output.time_series.std_energy = single
    sim.output.time_series.num_electrons = single
    sim.output.time_series.num_collisions = single
    sim.output.time_series.num_anions = single
    sim.output.time_series.num_cations = single
    sim.output.energy_distribution.generate_bins(
        sim.config.num_energy_bins, 5.3)
    return sim


def test_instantiation():
    Simulation('tests/data/config/test_config.json5')

    cfg = Config('tests/data/config/test_config.json5')
    cfg.initial_energy_distribution = "fixed"
    cfg.initial_energy = 5
    for d in ("random", [1, 1, 1]):
        cfg.initial_direction = d
        sim = Simulation(cfg.to_dict())
        assert np.isclose(sim.electrons.max_velocity_norm,
                          utils.velocity_from_energy(5))

    with pytest.raises(TypeError):
        Simulation(None)


def test_apply_config(sim):
    sim = copy.deepcopy(sim)
    cfg_dict = sim.config.to_dict()
    cfg_dict['output']['output_directory'] += 'new_directory/'
    sim.apply_config(cfg_dict)
    sim.save_pickle()


def test_save_pickle(sim):
    sim.save_pickle()


def test_advance_one_step(sim):
    sim.mc.calculate_max_coll_freq(sim.gas_mixture)
    sim.advance_one_step()


def test_collect_output_data(sim):
    sim.output.time_series.ind_equ = None
    sim.collect_output_data(1, 1, 1, 1)
    assert sim.output.time_series.ind_equ is not None
    sim.output.time_series.ind_equ = 0
    sim.collect_output_data(1, 1, 1, 1)


def get_callback(rv, args):
    if args is None:
        args = ()

    def callback(*a):
        assert args == a
        return rv

    return callback


def test_end(sim):
    sim.config.end_condition_type = "steady-state"
    sim.config.is_done = None
    of = sim.output.check_sst
    sim.output.check_sst = get_callback(True, None)
    assert sim.end_simulation()
    sim.output.check_sst = get_callback(False, None)
    assert not sim.end_simulation()
    sim.output.check_sst = of

    sim.config.end_condition_type = "w_tol+ND_tol"

    sim.output.flux.w = np.ones((3,)) * np.nan
    sim.output.flux.DN = np.ones((3,)) * np.nan
    sim.output.flux.w_err = np.zeros((3,)) * np.nan
    sim.output.flux.DN_err = np.zeros((3,)) * np.nan
    assert not sim.end_simulation()

    # condition on the convergence of flux w and flux DN
    sim.output.flux.w = np.ones((3,))
    sim.output.flux.DN = np.ones((3,))
    sim.output.flux.w_err = np.zeros((3,))
    sim.output.flux.DN_err = np.zeros((3,))
    assert sim.end_simulation()
    sim.output.flux.w_err = np.ones((3,))
    sim.output.flux.DN_err = np.ones((3,))
    assert not sim.end_simulation()

    sim.config.end_condition_type = "num_col_max"
    sim.output.time_series.num_collisions[-1] = sim.config.num_col_max - 1
    assert not sim.end_simulation()
    sim.output.time_series.num_collisions[-1] = sim.config.num_col_max
    assert sim.end_simulation()

    sim.config.end_condition_type = "custom"
    sim.config.is_done = get_callback(True, (sim,))
    assert sim.end_simulation()
    sim.config.is_done = get_callback(False, (sim,))
    assert not sim.end_simulation()

    sim.config.end_condition_type = "invalid"
    with pytest.raises(AssertionError):
        sim.end_simulation()

    sim.config.end_condition_type = "custom"
    sim.config.is_done = get_callback(False, (sim,))

    # no electrons
    sim.electrons.position = np.empty((3, 0))
    assert sim.end_simulation()

    # too many electrons
    sim.config.conserve = False
    sim.config.num_e_max = 100
    sim.electrons.position = np.empty((3, 100))
    assert not sim.end_simulation()
    assert sim.config.conserve

    sim.electrons.position = np.empty((3, 0))
    sim.config.conserve = True
    assert not sim.end_simulation()


def test_calculate_final_output(sim):
    sim.output.time_series.ind_equ = 0
    sim.output.energy_distribution.generate_bins(
        sim.config.num_energy_bins,
        1.2
    )
    sim.calculate_final_output()


def test_run(sim):
    sim.config.EN = 0
    sim.apply_config()
    sim.run()

    sim.config.seed = 0
    sim.apply_config()
    sim.run()


def test_run_series(sim):
    sim.config.EN = 0
    sim.apply_config()
    values = np.array([1e3, 1e5])
    sim.run_series('pressure', values)
    with pytest.raises(ValueError):
        sim.run_series('invalid_parameter', values)


def test_deterministic(sim):
    # for branch coverage
    sim.config.seed = "random"
    sim.apply_config()
    sim.mc.calculate_max_coll_freq(sim.gas_mixture)
    sim.advance_one_step()

    # sim.config.EN is modified by other test cases
    sim.config.seed = 15
    sim.apply_config()
    sim.mc.calculate_max_coll_freq(sim.gas_mixture)
    sim.advance_one_step()
    # spot check one value
    assert np.isclose(sim.electrons.energy[0], 0.1167247)


def test_timeout(sim):
    sim.config.timeout = 15

    def nothing():
        pass

    def is_done2(arg):
        return True

    def is_done1(arg):
        arg.config.is_done = is_done2
        return False

    def is_done1_sleep(arg):
        time.sleep(2)
        return False

    sim.advance_one_step = nothing
    sim.print_step_info = nothing
    sim.calculate_final_output = nothing
    sim.save = nothing

    sim.config.end_condition_type = "custom"
    sim.config.is_done = is_done1

    sim.run()
    assert sim.config.is_done == is_done2

    sim.config.timeout = 1
    sim.config.is_done = is_done1_sleep
    with pytest.warns(UserWarning, match="timeout after"):
        sim.run()
    assert sim.config.is_done == is_done1_sleep
