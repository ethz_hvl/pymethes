#  Copyright (c) 2020-2021 ETH Zurich

"""
Tests for the InstantaneousEnergyDistribution and TimeAveragedEnergyDistribution
classes.
"""

# Import Packages
import numpy as np

# Import modules
from pyMETHES.energy_distribution import (
    InstantaneousEnergyDistribution,
    TimeAveragedEnergyDistribution
)

np.seterr(all='raise')


def test_instantiation():
    InstantaneousEnergyDistribution()
    TimeAveragedEnergyDistribution()


def test_calculate_bin_centers():
    bins = np.linspace(0, 10, 6)
    centers = np.linspace(1, 9, 5)

    distri = InstantaneousEnergyDistribution()
    distri.energy_bins = bins
    distri.calculate_bin_centers()
    assert (distri.energy_bin_centers == centers).all()

    distri = TimeAveragedEnergyDistribution()
    distri.energy_bins = bins
    distri.calculate_bin_centers()
    assert (distri.energy_bin_centers == centers).all()


def test_instantaneous_calculate_distribution():

    distri = InstantaneousEnergyDistribution()
    energy = np.random.random(1000)

    distri.calculate_distribution(energy)
    assert distri.energy_bins is not None
    assert distri.energy_bin_centers is not None
    assert distri.eedf is not None
    assert distri.eepf is not None
    # check the normalization of the eedf
    assert np.isclose(sum(distri.eedf * np.diff(distri.energy_bins)), 1)

    distri = InstantaneousEnergyDistribution()
    energy = np.random.random(1000)
    bins = np.linspace(0, 1, 6)
    distri.calculate_distribution(energy, energy_bins=bins)
    assert (distri.energy_bins == bins).all()
    assert distri.energy_bin_centers is not None
    assert distri.eedf is not None
    assert distri.eepf is not None
    # check the normalization of the eedf
    assert np.isclose(sum(distri.eedf * np.diff(distri.energy_bins)), 1)


def test_averaged_collect_histogram():

    bins = np.linspace(0, 10, 6)
    centers = np.linspace(1, 9, 5)

    distri = TimeAveragedEnergyDistribution()
    distri.energy_bins = bins
    energy = np.repeat(centers, 10)

    distri.collect_histogram(energy)
    assert (distri.cumulative_energy_histogram == 10).all()


def test_averaged_calculate_distribution():

    distri = TimeAveragedEnergyDistribution()
    bins = np.linspace(0, 10, 6)
    centers = np.linspace(1, 9, 5)
    distri.energy_bins = bins
    distri.energy_bin_centers = centers
    distri.cumulative_energy_histogram = np.random.randint(0, 1000, centers.size)

    values = np.arange(100)
    distri.calculate_distribution(values)
    assert distri.eedf is not None
    assert distri.eepf is not None
    # check the normalization of the eedf
    assert np.isclose(sum(distri.eedf * np.diff(distri.energy_bins)), 1)

    assert np.isclose(distri.energy_mean, np.mean(values))
    assert np.isclose(distri.energy_mean_err, np.std(values)/10)
