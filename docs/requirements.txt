Sphinx>=2.2.0
sphinx-rtd-theme>=0.4.3
sphinxcontrib-mermaid>=0.4.0
pyMETHES
pip
setuptools
wheel
