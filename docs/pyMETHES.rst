pyMETHES package
================

Submodules
----------

pyMETHES.config module
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.config
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.cross\_section module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.cross_section
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.electrons module
~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.electrons
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.energy\_distribution module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.energy_distribution
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.gas\_mixture module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.gas_mixture
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.monte\_carlo module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.monte_carlo
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.output module
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.output
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.rate\_coefficients module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.rate_coefficients
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.simulation module
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.simulation
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.temporal\_evolution module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.temporal_evolution
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.transport\_data module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.transport_data
   :members:
   :undoc-members:
   :show-inheritance:

pyMETHES.utils module
~~~~~~~~~~~~~~~~~~~~~

.. automodule:: pyMETHES.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyMETHES
   :members:
   :undoc-members:
   :show-inheritance:
